schema([a,b,c,d,e]).

fds([
	[[a,b], [c]],
	[[c,d], [e]],
	[[b,e], [b]]
]).

answer(R, F) :- schema(R), fds(F), candkey(R, F, [a,b]).
answer2(R, F) :- schema(R), fds(F), candkey(R, F, [a,b,d]).
