schema([e]).	

fds1([
	[[a],[c]],
	[[a,c],[d]],
	[[e],[a,d]], 
	[[e],[h]]
]).

fds2([
	[[a], [c,d]],
	[[e], [a, h]]
]).	

answer(R, F1, F2) :- schema(R), fds1(F1), fds2(F2), equiv(R, F1, F2).
