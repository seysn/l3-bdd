schema([ename, ssn, bdate, address, dnumber, dname, dmgrssn]).

fds1([
	[[ssn],[ename, bdate, address, dnumber]],
	[[dnumber],[dname, dmgrssn]]
]).

answer1(R, F1, Xplus) :- schema(R), fds1(F1), xplus(R, F1, [ssn], Xplus).
answer2(R, F1, Xplus) :- schema(R), fds1(F1), xplus(R, F1, [dnumber], Xplus).
