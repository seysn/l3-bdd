schema([a,b,c,d,e,f,g,h,i]).

fds([
	[[a,b], [c]],
	[[a], [d,e]],
	[[b], [f]],
	[[f], [g,h]],
	[[d], [i,j]]
]).

answer(K) :- schema(R), fds(F), candkey(R, F, K).
answer3NF(R3NF) :- schema(R), fds(F), threenf(R, F, R3NF).
