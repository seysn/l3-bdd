TP5 : Base d’une compagnie aérienne
===================================

```sql
CREATE VIEW avions_vols
AS SELECT aid, vid
FROM avions, vols
WHERE portee >= distance;
```

Question 1
----------
```sql
SELECT eid, MAX(portee)
FROM employes NATURAL JOIN certifications NATURAL JOIN avions
GROUP BY eid
HAVING COUNT(aid) > 1;
```

Question 2
----------
```sql
SELECT enom
FROM employes, vols
WHERE dep = 'CDG' AND arr = 'NOU'
GROUP BY enom
HAVING BOOL_AND(salaire < prix);
```

Question 3
----------
```sql
SELECT DISTINCT dep, arr, distance
FROM employes NATURAL JOIN vols NATURAL JOIN avions_vols
WHERE salaire > 100000 ORDER BY dep;
```

```sql
SELECT vid
FROM vols
WHERE EXISTS (
	  
);
```

Question 4
----------
```sql
SELECT enom
FROM employes
```

Question 5
----------
```sql

```

Question 6
----------
```sql

```

Question 7
----------
```sql

```