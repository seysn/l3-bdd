TP4 : Requêtes imbriquées
=========================

Question 1
----------
```sql
SELECT acoul 
FROM Articles AS a1
WHERE NOT EXISTS (
	  SELECT *
	  FROM Articles AS a2
	  WHERE a1.aid <> a2.aid AND a2.acoul = a1.acoul
);
```

Question 2
----------
- EXISTS/NOT EXISTS
```sql
SELECT anom
FROM Articles AS a1
WHERE acoul = 'rouge' AND NOT EXISTS (
	  SELECT *
	  FROM Articles AS a2
	  WHERE a1.anom = a2.anom AND acoul = 'vert'
);
```

- ALL/SOME
```sql
SELECT anom
FROM Articles AS a1
WHERE acoul = 'rouge' AND anom <> ALL (
	  SELECT anom
	  FROM Articles AS a2
	  WHERE a1.anom = a2.anom AND acoul = 'vert'
);
```

- IN/NOT IN
```sql
SELECT anom
FROM Articles AS a1
WHERE acoul = 'rouge' AND anom NOT IN (
	  SELECT anom
	  FROM Articles AS a2
	  WHERE a1.anom = a2.anom AND acoul = 'vert'
);
```

Question 3
----------
Requete impossible mais voilà ce que j'ai fait (qui marche pas):
```sql
SELECT fnom
FROM Fournisseurs NATURAL JOIN Catalogue AS c1
WHERE prix > ALL (
	  SELECT AVG(prix)
	  FROM Catalogue AS c2
	  WHERE c1.fid = c2.fid
	  GROUP BY aid
);
```

Question 4
----------
```sql
SELECT DISTINCT anom
FROM Articles AS a1 NATURAL JOIN Catalogue AS c1
WHERE EXISTS (
	  SELECT *
	  FROM Articles AS a2 NATURAL JOIN Catalogue AS c2
	  WHERE a1.anom = a2.anom AND c1.fid <> c2.fid
);
```

Question 5
----------
- IN/NOT IN
```sql
SELECT fnom
FROM Fournisseurs
WHERE fid NOT IN (
	  SELECT fid FROM Catalogue
);
```

- ALL/SOME
```sql
SELECT fnom
FROM Fournisseurs
WHERE fid <> ALL (
	  SELECT fid FROM Catalogue
);
```

Question 6
----------
```sql
SELECT anom
FROM Articles NATURAL JOIN Catalogue AS c1 NATURAL JOIN Fournisseurs AS f1
WHERE fnom = 'kiventout' AND NOT EXISTS (
	  SELECT *
	  FROM Catalogue AS c2 NATURAL JOIN Fournisseurs AS f2
	  WHERE f1.fid <> f2.fid AND c1.aid = c2.aid
);
```

Question 7
----------
Marche pas parce que question IMPOSSIBLE !!!!!!!!!!!!!!!!!
```sql
SELECT fnom
FROM Catalogue AS c1 NATURAL JOIN Fournisseurs
WHERE NOT EXISTS (
	  SELECT *
	  FROM Catalogue AS c2
	  WHERE c1.aid = c2.aid
);
```

Question 8
----------
```sql
SELECT fnom
FROM Catalogue AS c1 NATURAL JOIN Fournisseurs
WHERE NOT EXISTS (
	  SELECT *
	  FROM Catalogue AS c2
	  WHERE c1.prix < c2.prix
);
```

Question 9
----------
```sql
SELECT anom, MIN(prix), MAX(prix)
FROM Articles NATURAL JOIN Catalogue AS c1
WHERE EXISTS (
	  SELECT *
	  FROM Catalogue AS c2
	  WHERE c1.fid <> c2.fid AND c1.aid = c2.aid
)
GROUP BY anom;
```

Question 10
-----------
```sql

```

Question 11
-----------
- EXISTS
```sql
SELECT anom
FROM Articles AS a1 NATURAL JOIN Catalogue AS c1
WHERE NOT EXISTS (
	  SELECT *
	  FROM Articles AS a2 NATURAL JOIN Catalogue AS c2
	  WHERE a1.anom = a2.anom AND c1.fid <> c2.fid
);
```

- GROUP BY
```sql
SELECT anom
FROM Articles NATURAL JOIN Catalogue
GROUP BY anom
HAVING COUNT(fid) = 1;
```

Question 12
-----------
- EXISTS
```sql

```

- ALL/SOME
```sql
SELECT DISTINCT aid
FROM Catalogue AS c1 NATURAL JOIN Articles
WHERE 100 < ALL (
	  SELECT prix
	  FROM Catalogue AS c2
	  WHERE c1.aid = c2.aid
);
```

Question 13
-----------
```sql
SELECT aid
FROM Catalogue AS c1 NATURAL JOIN Fournisseurs
WHERE EXISTS (
	  SELECT *
	  FROM Fournisseurs AS f1
	  WHERE f1.fid = c1.fid AND RIGHT(fad, 3) = 'USA'
);
```

Question 14
-----------
```sql

```

Question 15
-----------
```sql

```