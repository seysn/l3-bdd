TP6 - Premiers contacts avec Datalog
====================================

Exercice 1 : Histoires de jalousie
----------------------------------
Fichier ex1.dl

- Question 4
```
DES> femme(mia)

{                                           
  femme(mia)
}
Info: 1 tuple computed.

DES> femme(yolande)

{                                           
  femme(yolande)
}
Info: 1 tuple computed.

DES> femme(lapin)

{                                           
}
Info: 0 tuples computed.
```

- Question 5
```
DES> /listing femme

femme(jody).
femme(mia).
femme(yolande).

Info: 3 rules listed.
```

- Question 6
```
DES> aime(vincent, X), femme(X)

Info: Processing:
  answer(X) :-
    aime(vincent,X),
    femme(X).
{                                           
  answer(mia)
}
Info: 1 tuple computed. 
```

- Question 7
```
DES> aime(vincent, X), not(femme(X))

Info: Processing:
  answer(X) :-
    aime(vincent,X),
    not femme(X).
{                                           
  answer(pierre)
}
Info: 1 tuple computed.
```

- Question 8
```
DES> est_jaloux_de(vincent, X)

{                                           
  est_jaloux_de(vincent,marcellus),
  est_jaloux_de(vincent,vincent)
}
Info: 2 tuples computed.
```

- Question 9
```
DES> est_jaloux_de(vincent, _)

Info: Processing:
  answer :-
    est_jaloux_de(vincent,_).
{                                           
  answer
}
Info: 1 tuple computed.
```

Exercice 2 : La boutique
------------------------
Fichier createBoutique.dl

- Question 10
```
DES> couleur(X) :- fournisseurs(fid, _, _), articles(aid, _, X), catalogue(fid, aid, _).
DES> deuxrouges(F) :- fournisseurs(fid, F, _), articles(aid, _, 'rouge'), catalogue(fid, aid, _), count(fournisseurs(A, B, C), D), D > 1.
DES> liste(anom, fnom, prix) :- articles(aid, anom, acoul), fournisseurs(fid, fnom, fad), catalogue(fid, aid, prix).
DES> quivendquoi(anom, acoul, fid, prix) :- articles(aid, anom, acoul), catalogue(fid, aid, prix).
```


